import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt('pandaX_NR_events.txt')
E, S1, S2 = data[:,0], data[:,1], data[:,2]

sortE = sorted(E)
sortS1 = [y for x,y in sorted(zip( E, S1 ))]
sortS2 = [y for x,y in sorted(zip( E, S2 ))]


binsize=1.
step=1.
minE = 0.
maxE = 130.

x, Eff = [], []
aboveThresh, total = 0., 0.
for i in range(len(sortE)-1):
    if sortE[i] >= minE and sortE[i] < minE + step:
        total += 1.
        if sortS1[i] > 2. and sortS2[i] > 80.:
            if sortS1[i] < 135. and sortS2[i] < 20000.:
                aboveThresh += 1.
    if sortE[i+1] >= minE + step:
        x.append( minE + step/2. )
        Eff.append( aboveThresh / total )
        aboveThresh, total = 0., 0.
        minE += step
        if minE >= maxE:
            break


plt.plot( x, Eff, 'r' )
plt.xlim(0, 130)
plt.ylim(0, 1)
plt.xlabel('NR Energy [keV]', fontsize=16, family='serif')
plt.ylabel('Efficiency', fontsize=16, family='serif')
plt.tight_layout()
plt.show()
