import nestpy
import sys
import numpy as np
import matplotlib.pyplot as plt

'''
double TestSpectra::DD_spectrum(
    double xMin, double xMax) {  // JV LUX, most closely like JENDL-4. See
                                 // arXiv:1608.05381. Lower than G4/LUXSim
  if (xMax > 80.) xMax = 80.;
  if (xMin < 0.000) xMin = 0.000;
  double yMax = 1.1;
  vector<double> xyTry = {
    xMin + (xMax - xMin) * RandomGen::rndm()->rand_uniform(),
    yMax * RandomGen::rndm()->rand_uniform(), 1.};
  while (xyTry[2] > 0.) {
    double FuncValue = exp(-xyTry[0]/10.) + 0.1*exp(-pow((xyTry[0]-60.)/25.,2.));
    xyTry = RandomGen::rndm()->VonNeumann(xMin, xMax, 0., yMax, xyTry[0],
                                          xyTry[1], FuncValue);
  }
  return xyTry[0];
}
vector<double> RandomGen::VonNeumann(double xMin, double xMax, double yMin,
                                     double yMax, double xTest, double yTest,
                                     double fValue) {
  vector<double> xyTry(3);

  xyTry[0] = xTest;
  xyTry[1] = yTest;

  if (xyTry[1] > fValue) {
    xyTry[0] = xMin + (xMax - xMin) * RandomGen::rndm()->rand_uniform();
    xyTry[1] = yMin + (yMax - yMin) * RandomGen::rndm()->rand_uniform();
    xyTry[2] = 1.;
  } else
    xyTry[2] = 0.;

  return xyTry;  // doing a vector means you can return 2 values at the same
                 // time
}
'''

def DD_spectrum():
    yMax = 1.1
    xyTry = [ np.random.uniform(0., 80.), np.random.uniform(0., yMax), 1.]
    while xyTry[2] > 0.:
        FuncValue = np.exp(-xyTry[0]/10.) + 0.1*np.exp(-pow((xyTry[0]-60.)/25.,2.))
        
        '''start the von-nuemann generator'''
        if xyTry[1] < FuncValue:
            xyTry[0] = np.random.uniform(0., 80.)
            xyTry[1] = np.random.uniform(0., yMax)
            xyTry[2] = 1.
        else:
            xyTry[2] = 0.

        return xyTry[0]




def nestFullChain( interactionType, run_no ):
    detector = nestpy.PandaX_4T_Detector()
    
    #set custom detector gains:
    nc = nestpy.NESTcalc(detector)

    if interactionType == "NR":
      interaction = nestpy.INTERACTION_TYPE(0)  # NR
      eMax=120.
    else:
      interaction = nestpy.INTERACTION_TYPE(8) #beta ER
      eMax=55.

    xPos, yPos, zPos = 0., 0., 1185./2.
    
    #set lists for changing parameters
    e_life_times = [800.4, 939.2, 833.6, 1121.5, 1288.2]
    gate_voltages = [20., 18.6, 18., 16., 16.] #kV
    cathode_voltages = [4.9, 4.9, 5., 5., 5.]

    EEE = [0.902, 0.902, 92.6, 92.6, 92.6]
    SEG = [3.8, 3.8, 4.6, 4.6, 4.6]

    livetimes = [1.95, 13.25, 5.53, 35.58, 36.5]
    nEvents = int( livetimes[run_no-1]*50000. )

    density = nc.SetDensity( detector.get_T_Kelvin(), detector.get_p_bar() )
    print( 'Density = %.3f g/cc' % density )


    detector.set_eLife_us( e_life_times[run_no-1] )
    delta_V =  1000. * (gate_voltages[run_no-1] - cathode_voltages[run_no-1]) # Volts
    delta_L = (detector.get_gate()/10. - detector.get_cathode()/10. ) #cm
    field = delta_V / delta_L

    #detector.set_E_gas( 9.97 );
    #detector.set_g1_gas( float(sys.argv[1]) )
    detector.set_P_dphe( 0.20 )
    g1 = 0.09 / ( 1. + detector.get_P_dphe() );
    detector.set_g1( g1 )

    #optimize g1_gas....
    # we want to make sure the SE size matches PandaX in PE
    # but we don't know their DPE rate, so we'll guess and tune g1_gas
    # so that SE and g2 turn out correct in units of PE/e-
    g1_gas = detector.get_g1_gas()
    optimized = False;
    positive, negative = False, False
    step = 0.01
    first=True
    prev_res = -999
    while optimized==False:
        switched=False
        g1_gas += step
        detector.set_g1_gas( g1_gas )
        SE = nc.CalculateG2(False)[2] * (1. + detector.get_P_dphe() )
        res = abs(SEG[0] - SE)/SEG[0]
        #print( res, prev_res, step )
        if res < 0.001:
            optimized=True
            break;
        else:
            if SE > SEG[0]:
                postive = True
                if negative==True:
                    negative=False
                    switched=True
            if SE < SEG[0]:
                negative=True
                if positive==True:
                    positive=False
                    switched=True
        #print( positive, negative, switched )
        if first==False:
            if res > prev_res and switched==False:
                step *= -1.
            else:
                if switched==True:
                    step *= -0.5
        
        prev_res = res
        first=False


    print( 'Field = %.2f V/cm' % field )
    print( 'deltaV = %.2f V; deltaL = %.2f cm' % (delta_V, delta_L) )
    #Paper says liquid level changed, changing EEE and SEG, 
    # For EEE to change, Ext. Field needs to change as well
    if run_no > 2:
        detector.set_E_gas( 10.358 )
        detector.set_TopDrift( detector.get_TopDrift() -0.72 ) 
    g2Info = nc.CalculateG2(True)
    print("Using g1_gas %.4f" % detector.get_g1_gas() )
    #return 1
    # Get particle yields
    Es = []
    S1s, S2s = [], []
    for i in range(nEvents):
        #if interactionType == "NR":
        #    energy = DD_spectrum()
        #else:
        #    energy = np.random.uniform(0., eMax)
        energy = np.random.uniform(0., eMax)
        y = nc.GetYields(interaction, energy, drift_field = field)
        q = nc.GetQuanta(y)
    

        driftVelocity = nc.SetDriftVelocity(detector.get_T_Kelvin(), density, field )
        central_driftV = driftVelocity #assume uniform drift speed
        driftTime = (detector.get_TopDrift() - zPos) / driftVelocity

        S1 = nc.GetS1(q, xPos, yPos, zPos, xPos, yPos, zPos, driftVelocity, central_driftV, interaction, 0, field, energy, 0, False, [0], [0])
        S2 = nc.GetS2( q.electrons, xPos, yPos, zPos, xPos, yPos, zPos, driftTime, driftVelocity, 0, field, 0, False, [0], [0], g2Info )
        
        #if S1[3] > 0 and S2[5] > 0:
        S1s.append( S1[3] )
        #S2s.append( S2[7]/g2Info[3] )
        S2s.append( S2[5] ) 
        Es.append( energy )
        '''S1 = q.photons*0.09
        S2 = q.electrons*EEE[run_no-1]*SEG[run_no-1]
        if S1 > 0 and S2 > 0:
            S1s.append( S1 )
            S2s.append( S2 )

        '''


    return S1s, S2s, Es

#nestFullChain("ER", 1 )
#nestFullChain("ER", 5) 
#exit()
'''
Es, S1s, S2s = [], [], []
for i in [1, 2, 3, 4, 5]:
    thisS1, thisS2, Es = nestFullChain("ER", i )
    S1s += thisS1
    S2s += thisS2

erFile = open('pandaX_ER_events.txt', 'w' )
for i in range(len(S1s)):
    erFile.write( str(S1s[i])+'\t'+str(S2s[i])+'\n' )
erFile.close()



S1s = np.array(S1s)
S2s = np.array(S2s)

plt.plot(S1s, np.log10( S2s/S1s ), 'b o', ms=2, alpha=0.3 )
'''
Es, S1s, S2s = [], [], []
for i in [1, 2, 3, 4, 5]:
    thisS1, thisS2, thisE = nestFullChain("NR", i )
    S1s += thisS1
    S2s += thisS2
    Es += thisE

nrFile = open('pandaX_NR_events.txt', 'w' )
for i in range(len(S1s)):
    nrFile.write( str(Es[i])+'\t'+str(S1s[i])+'\t'+str(S2s[i])+'\n' )
nrFile.close()


'''
S1s = np.array(S1s)
S2s = np.array(S2s)

plt.plot(S1s, np.log10( S2s/S1s ), 'r o', ms=2, alpha=0.3 )


plt.xlim(0, 130)
plt.ylim(0., 2.5)
plt.show()

'''
